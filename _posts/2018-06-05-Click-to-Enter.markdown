---
layout: post
title:  "Click to Enter"
date:   2018-06-05 10:10:14 +0530
categories: post
---
Here are some of my active projects:

1. [chromium vaapi][chromiumva] <a href="https://copr.fedorainfracloud.org/coprs/hellbangerkarna/Chromium-Vaapi/package/chromium/"><img src="https://copr.fedorainfracloud.org/coprs/hellbangerkarna/Chromium-Vaapi/package/chromium/status_image/last_build.png" /></a><br>
[![pipeline status](https://gitlab.com/biswasab/chromium_vaapi_fed/badges/master/pipeline.svg)](https://gitlab.com/biswasab/chromium_vaapi_fed/commits/master)
Chromium browser built using clang and llvm, with video acceleration and other hardware acceleration patch included.

2. [ALPM][alpm] [![pipeline status](https://gitlab.com/biswasab/ALPM/badges/master/pipeline.svg)](https://gitlab.com/biswasab/ALPM/commits/master)
Advanced Linux Power Management script to save power on portable Linux devices.(Works with systemd)
This is a script which tweaks the hardware on Linux to save power whenever the system runs on battery by changing cpu schedulers and putting harddrives and Wifi adapters to enter into a low power state without touching or creating any conflict on the base system.

Other projects [here][others].


News: chromium-vaapi is on it's way to rpmfusion!






I accept donations [here][paypal]. Thank you very much for your support.





[chromiumva]: https://gitlab.com/biswasab/chromium_vaapi_fed
[alpm]: https://gitlab.com/biswasab/ALPM
[others]: https://gitlab.com/biswasab
[github]: https://github.com/biswasab
[paypal]: https://www.paypal.me/AkarshanBiswas